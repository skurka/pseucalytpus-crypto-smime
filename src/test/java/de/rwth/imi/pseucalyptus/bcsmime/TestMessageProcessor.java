package de.rwth.imi.pseucalyptus.bcsmime;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.function.Supplier;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSTypedStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestMessageProcessor {

	@Test
	public void decryptAndCompareCMSMessageResult() throws CMSException, IOException {
		PrivateKey key;
		try (InputStream in = getClass().getResourceAsStream("/b.private.pem")) {
			key = Utils.readPrivateKeyPEM(in);
		}

		try (InputStream in = getClass().getResourceAsStream("/output.b.openssl.asn1")) {
			CMSTypedStream result = MessageProcessor.decrypt(in, key);
			try (InputStream decrypted = result.getContentStream();
					InputStream original = getClass().getResourceAsStream("/message.b.txt")) {
				Assertions.assertArrayEquals(original.readAllBytes(), decrypted.readAllBytes());
			}
		}
	}

	@Test
	public void encryptWithCertificate()
			throws CMSException, IOException, CertificateException, NoSuchProviderException {
		// see
		// https://github.com/bcgit/bc-java/blob/master/mail/src/main/java/org/bouncycastle/mail/smime/examples/CreateLargeEncryptedMail.java
		Supplier<InputStream> data = () -> getClass().getResourceAsStream("/message.b.txt");

		X509Certificate cert;
		try (InputStream in = getClass().getResourceAsStream("/b.selfcert.pem")) {
			cert = Utils.readPublicKeyCertificatePEM(in);
		}

		byte[] encrypted = MessageProcessor.encrypt(data, cert);

		Assertions.assertNotNull(encrypted);
	}
	
	@Test
	public void wrapSMime() throws IOException {
		byte[] asn1 = getClass().getResourceAsStream("/output.b.openssl.asn1").readAllBytes();
		String smime = new String(getClass().getResourceAsStream("/output.b.openssl.smime").readAllBytes());
		Assertions.assertNotNull(asn1);
		Assertions.assertNotNull(smime);
		Assertions.assertEquals(smime, Utils.wrapAsn1InSMime(asn1));
	}
	
	@Test
	public void unwrapSMime() throws IOException {
		byte[] asn1 = getClass().getResourceAsStream("/output.b.openssl.asn1").readAllBytes();
		String smime = new String(getClass().getResourceAsStream("/output.b.openssl.smime").readAllBytes());
		Assertions.assertNotNull(asn1);
		Assertions.assertNotNull(smime);
		Assertions.assertArrayEquals(asn1, Utils.unwrapSMimeToAsn1(smime));
	}

}
