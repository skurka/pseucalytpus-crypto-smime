package de.rwth.imi.pseucalyptus.bcsmime;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

public class Utils {
	public static PrivateKey readPrivateKeyPEM(InputStream in) throws IOException {
		try( Reader reader = new InputStreamReader(in, StandardCharsets.US_ASCII) ){
			PEMParser pemParser = new PEMParser(reader);
			// TODO throw exception if types incompatible
			PEMKeyPair pair = (PEMKeyPair)pemParser.readObject();
			
	        JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
	        return (RSAPrivateKey) converter.getPrivateKey(pair.getPrivateKeyInfo());
			
		}		
	}
	// see https://gist.github.com/akorobov/6910564
	public static X509Certificate readPublicKeyCertificatePEM(InputStream in) throws IOException {
		try( Reader reader = new InputStreamReader(in, StandardCharsets.US_ASCII) ){
			// TODO throw exception if types incompatible
			PEMParser pemParser = new PEMParser(reader);
			
			X509CertificateHolder xh = (X509CertificateHolder)pemParser.readObject();
	        JcaX509CertificateConverter  xconv = new JcaX509CertificateConverter();
	        try {
				return xconv.getCertificate(xh);
			} catch (CertificateException e) {
				throw new IOException("Certificate processing failed",e);
			}
		}		
	}

	public static String wrapAsn1InSMime(byte[] asn1) {
		StringBuilder builder = new StringBuilder();
		builder.append("MIME-Version: 1.0\n");
		builder.append("Content-Disposition: attachment; filename=\"smime.p7m\"\n");
		builder.append("Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name=\"smime.p7m\"\n");
		builder.append("Content-Transfer-Encoding: base64\n");
		builder.append("\n");
		builder.append(new String(Base64.getMimeEncoder(64, new byte[] {0x0A}).encode(asn1),StandardCharsets.US_ASCII));
		builder.append("\n\n");
		return builder.toString();
	}
	
	public static byte[] unwrapSMimeToAsn1(String smime) {
		StringBuilder builder = new StringBuilder(smime);
		builder.delete(0, builder.indexOf("\n\n")+2);
		return Base64.getMimeDecoder().decode(builder.toString());
	}
}
