package de.rwth.imi.pseucalyptus.bcsmime;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.Supplier;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSTypedData;

import lombok.AllArgsConstructor;

@AllArgsConstructor
class TypedStreamPipe implements CMSTypedData {
	private ASN1ObjectIdentifier type;
	private Supplier<InputStream> data;

	public TypedStreamPipe(Supplier<InputStream> data) {
		type = new ASN1ObjectIdentifier("1.2.840.113549.1.7.1"); // id-data, this is what openssl uses
		this.data = data;
	}

	@Override
	public void write(OutputStream out) throws IOException, CMSException {
		try (InputStream in = data.get()) {
			in.transferTo(out);
		}
	}

	@Override
	public Object getContent() {
		return data;
	}

	@Override
	public ASN1ObjectIdentifier getContentType() {
		return type;
	}
}