package de.rwth.imi.pseucalyptus.bcsmime;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.function.Supplier;

import org.bouncycastle.cms.CMSAlgorithm;
import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSEnvelopedDataParser;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSTypedStream;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;


public class MessageProcessor {

	public static void main(String[] args) throws IOException {
		if (args.length != 5) {
			System.err
					.println("Usage: " + MessageProcessor.class.getName() + " encrypt|decrypt asn1|smime PEMfile infile outfile");
			System.exit(-1);
		}
		String mode = args[1];
		String pemfile = args[2];
		String infile = args[3];
		String outfile = args[4];

		if (args[0].equals("encrypt")) {
			X509Certificate cert;
			try (InputStream in = Files.newInputStream(Paths.get(pemfile))) {
				cert = Utils.readPublicKeyCertificatePEM(in);
			}
			byte[] input = Files.readAllBytes(Paths.get(infile));
			byte[] content = encrypt(input, cert);
			if(mode.equals("smime"))
				content = Utils.wrapAsn1InSMime(content).getBytes();
			try (OutputStream out = Files.newOutputStream(Paths.get(outfile))) {
				out.write(content);
			}
		} else if (args[0].equals("decrypt")) {
			PrivateKey key;
			try (InputStream in = Files.newInputStream(Paths.get(pemfile))) {
				key = Utils.readPrivateKeyPEM(in);
			}
			try (InputStream _in = Files.newInputStream(Paths.get(infile))) {
				InputStream in = _in;
				if(mode.equals("smime")) {
					in = new ByteArrayInputStream(Utils.unwrapSMimeToAsn1(new String(in.readAllBytes())));
				}
				CMSTypedStream content = decrypt(in, key);
				try (OutputStream out = Files.newOutputStream(Paths.get(outfile))) {
					content.getContentStream().transferTo(out);
				}
			}
		}
	}

	public static CMSTypedStream decrypt(InputStream asn1envelope, PrivateKey key) throws IOException {
		CMSEnvelopedDataParser parser;
		try {
			parser = new CMSEnvelopedDataParser(asn1envelope);
			RecipientInformationStore recipients = parser.getRecipientInfos();
			if (recipients.size() != 1) {
				throw new IllegalArgumentException("Unexpected number of recipients: " + recipients.size());
			}
			RecipientInformation rec = recipients.getRecipients().iterator().next();

			JceKeyTransEnvelopedRecipient x = new JceKeyTransEnvelopedRecipient(key);

			return rec.getContentStream(x);
		} catch (CMSException e) {
			throw new IOException("Decryption error", e);
		}
	}

	public static byte[] encrypt(Supplier<InputStream> content, X509Certificate cert) throws IOException {
		CMSEnvelopedDataGenerator gen = new CMSEnvelopedDataGenerator();

		JceKeyTransRecipientInfoGenerator rec;
		try {
			rec = new JceKeyTransRecipientInfoGenerator(cert);
			gen.addRecipientInfoGenerator(rec);
			CMSEnvelopedData out = gen.generate(new TypedStreamPipe(content),
					new JceCMSContentEncryptorBuilder(CMSAlgorithm.AES256_CBC).build());
			return out.getEncoded();
		} catch (CertificateEncodingException | CMSException e) {
			throw new IOException("Encryption error", e);
		}
	}

	public static byte[] encrypt(byte[] content, X509Certificate cert) throws IOException {
		return encrypt(() -> new ByteArrayInputStream(content), cert);
	}

}
